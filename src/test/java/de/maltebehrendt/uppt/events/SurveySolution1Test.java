package de.maltebehrendt.uppt.events;

import de.maltebehrendt.uppt.junit.AbstractTest;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.lang.invoke.MethodHandles;
import java.util.concurrent.ConcurrentLinkedQueue;

public class SurveySolution1Test extends AbstractTest {
    @BeforeClass
    public static void setup(TestContext context) {
        setupTestVertxAndDatabase(MethodHandles.lookup().lookupClass().getSimpleName(), context);
    }

    @AfterClass
    public static void teardown(TestContext context) {
        tearDownTestVertxAndCleanupDatabase(MethodHandles.lookup().lookupClass().getSimpleName(), context);
    }

    //@Test
    public void testSetSurvey(TestContext context) {
        Async testResults = context.async(5);

        ConcurrentLinkedQueue<String> documentKeyList = new ConcurrentLinkedQueue();

        // create questions
        for(int i=0;i<4;i++) {
            sendAsUser("in.survey", "1", "router", new JsonObject().put("requests", new JsonArray().add(new JsonObject()
                    .put("domain", "survey")
                    .put("version", "1")
                    .put("path", "$.questions")
                    .put("value", new JsonObject()
                            .put("questionType", "boolean")
                            .put("questionText", "Please select")
                            .put("answerPossibilities", new JsonArray())
                    )
                    .put("operation", "SET"))
            ), reply -> {
                context.assertFalse(reply.failed());
                context.assertEquals(200, reply.statusCode());

                String documentKey = reply.getBodyAsJsonObject().getJsonArray("results").getJsonObject(0).getJsonArray("values").getJsonObject(0).getString("_key");
                context.assertNotNull(documentKey);

                documentKeyList.add(documentKey);
                testResults.countDown();

                if(documentKeyList.size() == 4) {
                    // create survey
                    JsonObject survey = new JsonObject()
                            .put("description", "This is a test survey")
                            .put("pages", new JsonArray()
                                .add(new JsonObject()
                                        .put("questionKeys", new JsonArray()
                                                .add(documentKeyList.poll())
                                                .add(documentKeyList.poll())))
                                .add(new JsonObject()
                                        .put("questionKeys", new JsonArray()
                                                .add(documentKeyList.poll())
                                                .add(documentKeyList.poll())))
                            );

                    sendAsUser("in.survey", "1", "router", new JsonObject().put("requests", new JsonArray().add(new JsonObject()
                            .put("domain", "survey")
                            .put("version", "1")
                            .put("path", "$.surveys")
                            .put("value", survey)
                            .put("operation", "SET"))
                    ), surveyReply -> {
                        context.assertFalse(surveyReply.failed());
                        context.assertEquals(200, surveyReply.statusCode());

                        JsonObject body = surveyReply.getBodyAsJsonObject();
                        context.assertTrue(body.containsKey("results"));

                        JsonObject repliedSurvey = body.getJsonArray("results").getJsonObject(0).getJsonArray("values").getJsonObject(0);
                        context.assertNotNull(repliedSurvey);
                        context.assertEquals("This is a test survey", repliedSurvey.getString("description"));
                        String surveyKey = repliedSurvey.getString("_key");
                        context.assertNotNull(surveyKey);
                        JsonArray pages = repliedSurvey.getJsonArray("pages");
                        context.assertEquals(2, pages.size());
                        JsonObject firstPage = pages.getJsonObject(0);
                        context.assertTrue(firstPage.containsKey("questionKeys"));
                        context.assertEquals(2, firstPage.getJsonArray("questionKeys").size());

                        testResults.countDown();
                    });
                }
            });
        }

        testResults.awaitSuccess();
    }

    //@Test
    public void testGetFullSurvey(TestContext context) {
        Async testResults = context.async(6);

        ConcurrentLinkedQueue<String> documentKeyList = new ConcurrentLinkedQueue();

        // create questions
        for(int i=0;i<4;i++) {
            sendAsUser("in.survey", "1", "router", new JsonObject().put("requests", new JsonArray().add(new JsonObject()
                    .put("domain", "survey")
                    .put("version", "1")
                    .put("path", "$.questions")
                    .put("value", new JsonObject()
                            .put("questionType", "boolean")
                            .put("questionText", "Please select")
                            .put("answerPossibilities", new JsonArray())
                    )
                    .put("operation", "SET"))
            ), reply -> {
                context.assertFalse(reply.failed());
                context.assertEquals(200, reply.statusCode());

                String documentKey = reply.getBodyAsJsonObject().getJsonArray("results").getJsonObject(0).getJsonArray("values").getJsonObject(0).getString("_key");
                context.assertNotNull(documentKey);

                documentKeyList.add(documentKey);
                testResults.countDown();

                if(documentKeyList.size() == 4) {
                    // create survey
                    JsonObject survey = new JsonObject()
                            .put("description", "This is a test survey")
                            .put("pages", new JsonArray()
                                    .add(new JsonObject()
                                            .put("questionKeys", new JsonArray()
                                                    .add(documentKeyList.poll())
                                                    .add(documentKeyList.poll())))
                                    .add(new JsonObject()
                                            .put("questionKeys", new JsonArray()
                                                    .add(documentKeyList.poll())
                                                    .add(documentKeyList.poll())))
                            );

                    sendAsUser("in.survey", "1", "router", new JsonObject().put("requests", new JsonArray().add(new JsonObject()
                            .put("domain", "survey")
                            .put("version", "1")
                            .put("path", "$.surveys")
                            .put("value", survey)
                            .put("operation", "SET"))
                    ), surveyReply -> {
                        context.assertFalse(surveyReply.failed());
                        context.assertEquals(200, surveyReply.statusCode());

                        JsonObject repliedSurvey = surveyReply.getBodyAsJsonObject().getJsonArray("results").getJsonObject(0).getJsonArray("values").getJsonObject(0);
                        String surveyKey = repliedSurvey.getString("_key");
                        testResults.countDown();

                        sendAsUser("in.survey", "1", "router", new JsonObject().put("requests", new JsonArray().add(new JsonObject()
                                .put("domain", "survey")
                                .put("version", "1")
                                .put("path", "$.surveys." + surveyKey)
                                .put("operation", "GET"))
                        ), getReply -> {
                            context.assertFalse(getReply.failed());
                            context.assertEquals(200, getReply.statusCode());

                            JsonArray values = getReply.getBodyAsJsonObject().getJsonArray("results").getJsonObject(0).getJsonArray("values");
                            JsonArray paths = getReply.getBodyAsJsonObject().getJsonArray("results").getJsonObject(0).getJsonArray("paths");

                            JsonObject fullSurvey = values.getJsonObject(0);
                            context.assertNotNull(fullSurvey);
                            context.assertEquals("This is a test survey", fullSurvey.getString("description"));
                            String fullSurveyKey = fullSurvey.getString("_key");
                            context.assertNotNull(fullSurveyKey);
                            JsonArray pages = fullSurvey.getJsonArray("pages");
                            context.assertEquals(2, pages.size());
                            JsonObject firstPage = pages.getJsonObject(0);
                            context.assertTrue(firstPage.containsKey("questionKeys"));
                            context.assertEquals(2, firstPage.getJsonArray("questionKeys").size());

                            context.assertEquals(5, values.size());
                            context.assertEquals(5, paths.size());

                            testResults.countDown();
                        });
                    });
                }
            });
        }

        testResults.awaitSuccess();
    }

    @Test
    public void testSetQuestion(TestContext context) {
        Async testResults = context.async(4);

        for(int i=0;i<4;i++) {
            sendAsUser("in.survey", "1", "router", new JsonObject().put("requests", new JsonArray().add(new JsonObject()
                    .put("domain", "survey")
                    .put("version", "1")
                    .put("path", "$.questions")
                    .put("value", new JsonObject()
                            .put("questionType", "boolean")
                            .put("questionText", "Please select")
                            .put("answerPossibilities", new JsonArray())
                    )
                    .put("operation", "SET"))
            ), reply -> {
                context.assertFalse(reply.failed());
                context.assertEquals(200, reply.statusCode());

                JsonObject body = reply.getBodyAsJsonObject();
                context.assertTrue(body.containsKey("results"));
                JsonArray results = body.getJsonArray("results");
                context.assertEquals(1, results.size());
                context.assertTrue(results.getJsonObject(0).containsKey("values"));
                context.assertTrue(results.getJsonObject(0).containsKey("paths"));
                testResults.countDown();
            });
        }

        testResults.awaitSuccess();
    }

    @Test
    public void testGetQuestion(TestContext context) {
        Async testResults = context.async(2);

        // create question
        sendAsUser("in.survey", "1", "router", new JsonObject().put("requests", new JsonArray().add(new JsonObject()
                .put("domain", "survey")
                .put("version", "1")
                .put("path", "$.questions")
                .put("value", new JsonObject()
                        .put("questionType", "boolean")
                        .put("questionText", "Please select")
                        .put("answerPossibilities", new JsonArray())
                )
                .put("operation", "SET"))
        ), reply -> {
            context.assertFalse(reply.failed());
            context.assertEquals(200, reply.statusCode());

            String documentKey = reply.getBodyAsJsonObject().getJsonArray("results").getJsonObject(0).getJsonArray("values").getJsonObject(0).getString("_key");
            context.assertNotNull(documentKey);

            sendAsUser("in.survey", "1", "router", new JsonObject().put("requests", new JsonArray().add(new JsonObject()
                            .put("domain", "survey")
                            .put("version", "1")
                            .put("path", "$.questions." + documentKey)
                            .put("operation", "GET"))
                    ), getReply -> {
                        context.assertEquals(200, getReply.statusCode());

                        JsonObject body = reply.getBodyAsJsonObject();
                        context.assertTrue(body.containsKey("results"));
                        JsonArray results = body.getJsonArray("results");
                        context.assertEquals(1, results.size());
                        JsonObject value = results.getJsonObject(0).getJsonArray("values").getJsonObject(0);
                        String path = results.getJsonObject(0).getJsonArray("paths").getString(0);
                        context.assertEquals(value.getString("_key"), documentKey);
                        context.assertTrue(value.containsKey("questionType"));
                        context.assertTrue(value.containsKey("questionText"));
                        context.assertTrue(value.containsKey("answerPossibilities"));
                        context.assertEquals("$.questions." + documentKey, path);
                        testResults.countDown();
                    });


            testResults.countDown();
        });


        testResults.awaitSuccess();
    }

    @Test
    public void testGetAllQuestions(TestContext context) {
        Async testResults = context.async(9);
        ConcurrentLinkedQueue<String> documentKeyList = new ConcurrentLinkedQueue();

        // create questions
        for(int i=0;i<4;i++) {
            sendAsUser("in.survey", "1", "router", new JsonObject().put("requests", new JsonArray().add(new JsonObject()
                    .put("domain", "survey")
                    .put("version", "1")
                    .put("path", "$.questions")
                    .put("value", new JsonObject()
                            .put("questionType", "boolean")
                            .put("questionText", "Please select")
                            .put("answerPossibilities", new JsonArray())
                    )
                    .put("operation", "SET"))
            ), reply -> {
                context.assertFalse(reply.failed());
                context.assertEquals(200, reply.statusCode());

                String documentKey = reply.getBodyAsJsonObject().getJsonArray("results").getJsonObject(0).getJsonArray("values").getJsonObject(0).getString("_key");
                context.assertNotNull(documentKey);

                documentKeyList.add(documentKey);
                testResults.countDown();


                if(documentKeyList.size() == 4) {
                    sendAsUser("in.survey", "1", "router", new JsonObject().put("requests", new JsonArray().add(new JsonObject()
                            .put("domain", "survey")
                            .put("version", "1")
                            .put("path", "$.questions.*")
                            .put("operation", "GET"))
                    ), getReply -> {
                        context.assertEquals(200, getReply.statusCode());
                        JsonObject body = getReply.getBodyAsJsonObject();

                        context.assertTrue(body.containsKey("results"));
                        JsonArray results = body.getJsonArray("results");
                        context.assertEquals(1, results.size());
                        JsonArray values = results.getJsonObject(0).getJsonArray("values");
                        context.assertTrue(values.size() >= 4);
                        testResults.countDown();

                        documentKeyList.forEach(key -> {
                            boolean isDocumentMissing = true;
                            for(int a=0;a<values.size() && isDocumentMissing;a++) {
                                isDocumentMissing = key.equals(values.getJsonObject(a).getString("_key"));
                            }
                            context.assertFalse(isDocumentMissing);

                            testResults.countDown();
                        });
                    });
                }
            });
        }
        testResults.awaitSuccess();
    }

    //@Test
    public void testDeleteQuestion(TestContext context) {
        Async testResults = context.async(3);

        // create question
        sendAsUser("in.survey", "1", "router", new JsonObject().put("requests", new JsonArray().add(new JsonObject()
                .put("domain", "survey")
                .put("version", "1")
                .put("path", "$.questions")
                .put("value", new JsonObject()
                        .put("questionType", "boolean")
                        .put("questionText", "Please select")
                        .put("answerPossibilities", new JsonArray())
                )
                .put("operation", "SET"))
        ), reply -> {
            context.assertFalse(reply.failed());
            context.assertEquals(200, reply.statusCode());

            String documentKey = reply.getBodyAsJsonObject().getJsonArray("results").getJsonObject(0).getJsonArray("values").getJsonObject(0).getString("_key");
            context.assertNotNull(documentKey);

            sendAsUser("in.survey", "1", "router", new JsonObject().put("requests", new JsonArray().add(new JsonObject()
                    .put("domain", "survey")
                    .put("version", "1")
                    .put("path", "$.questions." + documentKey)
                    .put("operation", "DEL"))
            ), delReply -> {
                context.assertEquals(200, delReply.statusCode());

                JsonObject body = delReply.getBodyAsJsonObject();
                context.assertTrue(body.containsKey("results"));
                JsonArray results = body.getJsonArray("results");
                context.assertEquals(1, results.size());
                JsonObject value = results.getJsonObject(0).getJsonArray("values").getJsonObject(0);
                String path = results.getJsonObject(0).getJsonArray("paths").getString(0);
                context.assertEquals(value.getString("_key"), documentKey);
                context.assertTrue(value.containsKey("questionType"));
                context.assertTrue(value.containsKey("questionText"));
                context.assertTrue(value.containsKey("answerPossibilities"));
                context.assertEquals("$.questions." + documentKey, path);
                testResults.countDown();

                sendAsUser("in.survey", "1", "router", new JsonObject().put("requests", new JsonArray().add(new JsonObject()
                        .put("domain", "survey")
                        .put("version", "1")
                        .put("path", "$.questions." + documentKey)
                        .put("operation", "GET"))
                ), getReply -> {
                    context.assertEquals(500, getReply.statusCode());
                    testResults.countDown();
                });
            });
            testResults.countDown();
        });


        testResults.awaitSuccess();
    }


    @Test
    public void testSetQuestionChecksRequiredFieldsExist(TestContext context) {
        Async testResults = context.async(4);

        sendAsUser("in.survey", "1", "router", new JsonObject().put("requests", new JsonArray().add(new JsonObject()
                .put("domain", "survey")
                .put("version", "1")
                .put("path", "$.questions")
                .put("value", new JsonObject())
                .put("operation", "SET"))
        ), reply -> {
            context.assertEquals(500, reply.statusCode());
            testResults.countDown();
        });

        sendAsUser("in.survey", "1", "router", new JsonObject().put("requests", new JsonArray().add(new JsonObject()
                .put("domain", "survey")
                .put("version", "1")
                .put("path", "$.questions")
                .put("value", new JsonObject()
                        .put("questionText", "Please select")
                        .put("answerPossibilities", new JsonArray())
                    )
                .put("operation", "SET"))
        ), reply -> {
            context.assertEquals(500, reply.statusCode());
            testResults.countDown();
        });

        sendAsUser("in.survey", "1", "router", new JsonObject().put("requests", new JsonArray().add(new JsonObject()
                .put("domain", "survey")
                .put("version", "1")
                .put("path", "$.questions")
                .put("value", new JsonObject()
                        .put("questionType", "boolean")
                        .put("answerPossibilities", new JsonArray())
                )
                .put("operation", "SET"))
        ), reply -> {
            context.assertEquals(500, reply.statusCode());
            testResults.countDown();
        });

        sendAsUser("in.survey", "1", "router", new JsonObject().put("requests", new JsonArray().add(new JsonObject()
                .put("domain", "survey")
                .put("version", "1")
                .put("path", "$.questions")
                .put("value", new JsonObject()
                        .put("questionType", "boolean")
                        .put("questionText", "Please select")
                )
                .put("operation", "SET"))
        ), reply -> {
            context.assertEquals(500, reply.statusCode());
            testResults.countDown();
        });

        testResults.awaitSuccess();
    }

}
