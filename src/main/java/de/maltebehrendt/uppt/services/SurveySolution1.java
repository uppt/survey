package de.maltebehrendt.uppt.services;

import de.maltebehrendt.uppt.annotation.Authorities;
import de.maltebehrendt.uppt.annotation.Route;
import de.maltebehrendt.uppt.annotation.Router;
import de.maltebehrendt.uppt.enums.Operation;
import de.maltebehrendt.uppt.events.Message;
import io.vertx.core.Future;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.util.HashMap;
import java.util.UUID;

public class SurveySolution1 extends AbstractSolution {
    private static String questionSetQuery = "FOR u IN users FILTER u.userID == @userID LET question = (INSERT @questionDocument IN questions RETURN NEW) INSERT {_from: u._id, _to: FIRST(question)._id } IN owns RETURN FIRST(question)";
    private static String questionGetQuery = "FOR u IN users FILTER u.userID == @userID LET question = (FOR q IN OUTBOUND u._id owns FILTER q._key == @questionKey FILTER IS_SAME_COLLECTION('questions', q._id) RETURN q) RETURN FIRST(question)";
    private static String questionsGetAllQuery = "FOR u IN users FILTER u.userID == @userID LET qs = (FOR q IN OUTBOUND u._id owns FILTER IS_SAME_COLLECTION('questions', q._id) RETURN q) RETURN {questions: qs}";
    private static String questionDelQuery = "FOR u IN users FILTER u.userID == @userID LET question = (FOR q IN OUTBOUND u._id owns FILTER q._key == @questionKey FILTER IS_SAME_COLLECTION('questions', q._id) RETURN q) REMOVE question._key IN questions RETURN OLD";

    private static String surveySetQuery = "FOR u IN users FILTER u.userID == @userID LET survey = (INSERT @surveyDocument IN surveys RETURN NEW) INSERT {_from: u._id, _to: FIRST(survey)._id } IN owns RETURN FIRST(survey)";
    private static String surveyGetQuery = "FOR u IN users FILTER u.userID == @userID LET survey = (FOR s IN OUTBOUND u._id owns FILTER s._key == @surveyKey FILTER IS_SAME_COLLECTION('surveys', s._id) RETURN s) RETURN FIRST(question)";
    private static String surveysGetAllQuery = "FOR u IN users FILTER u.userID == @userID LET ss = (FOR s IN OUTBOUND u._id owns FILTER IS_SAME_COLLECTION('surveys', s._id) RETURN s) RETURN {surveys: ss}";
    private static String surveyDelQuery = "FOR u IN users FILTER u.userID == @userID LET survey = (FOR s IN OUTBOUND u._id owns FILTER s._key == @surveyKey FILTER IS_SAME_COLLECTION('surveys', s._id) RETURN s) REMOVE survey._key IN surveys RETURN OLD";


    private JsonObject tmpMap = new JsonObject();

    @Router(domain = "in.survey",
            version = "1",
            type = "router",
            description = "Router for the the survey solution",
            authorities = @Authorities(
                    isAuthenticationRequired = false
            )
    )
    @Override
    public void router(Message message) {
        // don't return the requests for saving a little bandwidth
        message.reply(new JsonObject().put("results", message.getBodyAsJsonObject().getJsonArray("results")));
    }


    @Route(
            description = "Creates/updates survey(s)",
            domain = "survey",
            version = "1",
            path = "surveys",
            operation = Operation.SET
    )
    public void surveysSet(Message message) {
        JsonObject msg = message.getBodyAsJsonObject();
        String userID = msg.getString("userID");
        JsonObject value = msg.getJsonObject("value");

        // check for minimum fields
        if(!value.containsKey("description")) {
            message.fail(400, "Must provide description");
            return;
        }
        if(!value.containsKey("pages")) {
            message.fail(400, "Must provide pages");
            return;
        }

        JsonArray pages = value.getJsonArray("pages");
        if(pages.isEmpty()) {
            message.fail(400, "Must provide at least one page");
            return;
        }

        for(int i = 0;i<pages.size();i++) {
            JsonObject page = pages.getJsonObject(i);
            if(!page.containsKey("questionKeys") || page.getJsonArray("questionKeys").isEmpty()) {
                message.fail(400, "Each page must contain questionKeys");
                return;
            }
        }

        Future<JsonArray> queryFuture = Future.future();
        queryFuture.setHandler(handler -> {
            if(handler.failed()) {
                message.fail("Failed to set survey: " + handler.cause());
                return;
            }
            JsonObject survey =  handler.result().getJsonObject(0);
            String surveyKey = survey.getString("_key");

            message.reply(new JsonObject()
                    .put("paths", new JsonArray().add("$.surveys." + surveyKey))
                    .put("values", new JsonArray().add(survey))
            );
        });

        HashMap<String, Object> params = new HashMap<>();
        params.put("userID", userID);
        database.query(surveySetQuery, params, queryFuture);

        if(!tmpMap.containsKey("surveys")) {
            tmpMap.put("surveys", new JsonObject());
        }
        if(!tmpMap.getJsonObject("surveys").containsKey(userID)) {
            tmpMap.getJsonObject("surveys").put(userID, new JsonObject());
        }

        String documentKey = "_" + UUID.randomUUID().toString();
        value.put("_key", documentKey);
        tmpMap.getJsonObject("surveys").getJsonObject(userID).put(documentKey, value);

        message.reply(200, new JsonObject()
                .put("paths", new JsonArray().add("$.surveys." + documentKey))
                .put("values", new JsonArray().add(value))
        );
    }

    @Route(
            description = "Retrieve survey(s)",
            domain = "survey",
            version = "1",
            path = "surveys.*",
            operation = Operation.GET
    )
    public void surveysGet(Message message) {
        JsonObject msg = message.getBodyAsJsonObject();
        JsonArray pathSteps = msg.getJsonArray("pathSteps");
        JsonArray pathPredicates = msg.getJsonArray("pathPredicates");

        if(pathSteps.size() != 2) {
            message.fail("Provided path structure/type is not implemented");
            return;
        }

        JsonArray selectorPredicate = pathPredicates.getJsonArray(1);

        JsonArray surveys = new JsonArray();
        JsonArray paths = new JsonArray();
        JsonArray values = new JsonArray();

        if(selectorPredicate.isEmpty()) {
            String documentKey = pathSteps.getString(1);
            JsonObject survey = tmpMap.getJsonObject("surveys").getJsonObject(msg.getString("userID")).getJsonObject(documentKey);

            if(survey == null) {
                message.fail(404, "Survey with key " + documentKey + " not found");
            }

            surveys.add(survey);
        }
        else if("*".equals(selectorPredicate.getJsonObject(0).getString("value"))) {
            JsonObject surveyMap = tmpMap.getJsonObject("questions").getJsonObject(msg.getString("userID"));

            for(String documentKey: surveyMap.fieldNames()) {
                surveys.add(surveyMap.getJsonObject(documentKey));
            }
        }
        else {
            message.fail("Provided path is not implemented");
            return;
        }

        for(int b=0;b<surveys.size();b++) {
            JsonObject survey = surveys.getJsonObject(b);

            paths.add("$.surveys." + survey.getString("_key"));
            values.add(survey);

            JsonArray pages = survey.getJsonArray("pages");
            for(int i = 0;i<pages.size();i++) {
                JsonArray questionKeys = pages.getJsonObject(i).getJsonArray("questionKeys");
                for(int a=0;a<questionKeys.size();a++) {
                    String questionKey = questionKeys.getString(a);
                    JsonObject question = tmpMap.getJsonObject("questions").getJsonObject(msg.getString("userID")).getJsonObject(questionKey);

                    if(question == null) {
                        message.fail(404, "Question with key " + questionKey + " of survey " + survey.getString("_key") + " not found");
                        return;
                    }

                    paths.add("$.questions." + questionKey);
                    values.add(question);
                }
            }
        }

        message.reply(new JsonObject()
                .put("paths", paths)
                .put("values", values)
        );
    }

    @Route(
            description = "Delete survey(s)",
            domain = "survey",
            version = "1",
            path = "surveys.*",
            operation = Operation.DEL
    )
    public void surveysDel(Message message) {
        JsonObject msg = message.getBodyAsJsonObject();
        JsonArray pathSteps = msg.getJsonArray("pathSteps");
        JsonArray pathPredicates = msg.getJsonArray("pathPredicates");

        if(pathSteps.size() != 2) {
            message.fail("Provided path structure/type is not implemented");
            return;
        }

        JsonArray selectorPredicate = pathPredicates.getJsonArray(1);
        if(selectorPredicate.isEmpty()) {
            String documentKey = pathSteps.getString(1);
            JsonObject question = tmpMap.getJsonObject("surveys").getJsonObject(msg.getString("userID")).getJsonObject(documentKey);

            tmpMap.getJsonObject("surveys").getJsonObject(msg.getString("userID")).remove(documentKey);

            message.reply(new JsonObject()
                    .put("paths", new JsonArray().add("$.surveys." + documentKey))
                    .put("values", new JsonArray().add(question))
            );
        }
        else if("*".equals(selectorPredicate.getJsonObject(0).getString("value"))) {
            JsonObject questions = tmpMap.getJsonObject("surveys").getJsonObject(msg.getString("userID"));
            JsonArray values = new JsonArray();
            JsonArray paths = new JsonArray();

            for(String documentKey: questions.fieldNames()) {
                paths.add("$.surveys." + documentKey);
                values.add(questions.getJsonObject(documentKey));
            }

            tmpMap.getJsonObject("surveys").getJsonObject(msg.getString("userID")).clear();

            message.reply(new JsonObject()
                    .put("paths", paths)
                    .put("values", values)
            );
        }
        else {
            message.fail("Provided path is not implemented");
        }
    }

    @Route(
            description = "Creates/updates question(s)",
            domain = "survey",
            version = "1",
            path = "questions",
            operation = Operation.SET
    )
    public void questionSet(Message message) {
        JsonObject msg = message.getBodyAsJsonObject();
        String userID = msg.getString("userID");
        JsonObject value = msg.getJsonObject("value");

        // check for minimum fields
        if(!value.containsKey("questionType")) {
            message.fail(400, "Must provide questionType. E.g. Likert, Boolean, MultipleChoice, Text");
            return;
        }
        if(!value.containsKey("questionText")) {
            message.fail(400,"Must provide questionText (question shown to user)");
            return;
        }
        if(!value.containsKey("answerPossibilities")) {
            message.fail(400, "Must provide answerPossibilities");
            return;
        }

        Future<JsonArray> queryFuture = Future.future();
        queryFuture.setHandler(handler -> {
            if(handler.failed()) {
                message.fail("Failed to set question: " + handler.cause());
                return;
            }
            JsonObject question =  handler.result().getJsonObject(0);
            String documentKey = question.getString("_key");

            message.reply(new JsonObject()
                    .put("paths", new JsonArray().add("$.questions." + documentKey))
                    .put("values", new JsonArray().add(question))
            );
        });

        HashMap<String, Object> params = new HashMap<>();
        params.put("userID", userID);
        params.put("questionDocument", value.getMap());
        database.query(questionSetQuery, params, queryFuture);
    }

    @Route(
            description = "Retrieve question(s)",
            domain = "survey",
            version = "1",
            path = "questions.*",
            operation = Operation.GET
    )
    public void questionGet(Message message) {
        JsonObject msg = message.getBodyAsJsonObject();
        JsonArray pathSteps = msg.getJsonArray("pathSteps");
        JsonArray pathPredicates = msg.getJsonArray("pathPredicates");

        if(pathSteps.size() != 2) {
            message.fail("Provided path structure/type is not implemented");
            return;
        }

        JsonArray selectorPredicate = pathPredicates.getJsonArray(1);
        if(selectorPredicate.isEmpty()) {
            String documentKey = pathSteps.getString(1);
            Future<JsonArray> queryFuture = Future.future();
            queryFuture.setHandler(handler -> {
               if(handler.failed()) {
                   message.fail("Failed to get question with id " + documentKey + ": " + handler.cause());
                   return;
               }

               JsonObject question = handler.result().getJsonObject(0);

               if(question == null) {
                   message.fail(404, "Question with key " + documentKey + " not found");
                   return;
               }

               message.reply(new JsonObject()
                       .put("paths", new JsonArray().add("$.questions." + documentKey))
                       .put("values", new JsonArray().add(question))
               );
            });

            HashMap<String, Object> params = new HashMap<>();
            params.put("userID", msg.getString("userID"));
            params.put("questionKey", documentKey);
            database.query(questionGetQuery, params, queryFuture);
        }
        else if("*".equals(selectorPredicate.getJsonObject(0).getString("value"))) {
            Future<JsonArray> queryFuture = Future.future();
            queryFuture.setHandler(handler -> {
                if(handler.failed()) {
                    message.fail("Failed to get questions: " + handler.cause());
                    return;
                }

                JsonArray questions = handler.result().getJsonObject(0).getJsonArray("questions");
                JsonArray paths = new JsonArray();

                if(questions == null) {
                    questions = new JsonArray();
                }

                for(int i=0;i<questions.size();i++) {
                    paths.add("$.questions." + questions.getJsonObject(i).getString("_key"));
                }

                message.reply(new JsonObject()
                        .put("paths",paths)
                        .put("values", questions)
                );
            });

            HashMap<String, Object> params = new HashMap<>();
            params.put("userID", msg.getString("userID"));
            database.query(questionsGetAllQuery, params, queryFuture);
        }
        else {
            message.fail("Provided path is not implemented");
        }
    }

    @Route(
            description = "Delete question(s)",
            domain = "survey",
            version = "1",
            path = "questions.*",
            operation = Operation.DEL
    )
    public void questionDel(Message message) {
        JsonObject msg = message.getBodyAsJsonObject();
        JsonArray pathSteps = msg.getJsonArray("pathSteps");
        JsonArray pathPredicates = msg.getJsonArray("pathPredicates");

        if(pathSteps.size() != 2) {
            message.fail("Provided path structure/type is not implemented");
            return;
        }

        JsonArray selectorPredicate = pathPredicates.getJsonArray(1);
        if(selectorPredicate.isEmpty()) {
            String documentKey = pathSteps.getString(1);
            Future<JsonArray> queryFuture = Future.future();
            queryFuture.setHandler(handler -> {
                if(handler.failed()) {
                    message.fail("Failed to delete question with id " + documentKey + ": " + handler.cause());
                    return;
                }

                JsonObject question = handler.result().getJsonObject(0);

                if(question == null) {
                    message.fail(404, "Question with key " + documentKey + " not found");
                    return;
                }

                message.reply(new JsonObject()
                        .put("paths", new JsonArray().add("$.questions." + documentKey))
                        .put("values", new JsonArray().add(question))
                );
            });

            HashMap<String, Object> params = new HashMap<>();
            params.put("userID", msg.getString("userID"));
            params.put("questionKey", documentKey);
            database.query(questionDelQuery, params, queryFuture);
        }
        else {
            message.fail("Provided path is not implemented");
        }
    }


    @Override
    public void processSessionConnected(String userID, JsonArray userRoles, String sessionAddress) {
        processSessionAuthenticated(userID, userRoles, sessionAddress);
    }

    @Override
    public void processSessionDisconnected(String userID, JsonArray userRoles, String sessionAddress) {}

    @Override
    public void processSessionAuthenticated(String userID, JsonArray userRoles, String sessionAddress) {
        // TODO: send survey status/id/session information
    }

    @Override
    public void prepare(Future<Object> prepareFuture) {
        prepareFuture.complete();
    }

    @Override
    public void shutdown(Future<Object> shutdownFuture) {
        shutdownFuture.complete();
    }
}
